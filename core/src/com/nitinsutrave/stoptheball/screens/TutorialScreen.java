package com.nitinsutrave.stoptheball.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.nitinsutrave.stoptheball.StopTheBall;
import com.nitinsutrave.stoptheball.helpers.AssetLoader;
import com.nitinsutrave.stoptheball.utils.GameConstants;
import com.nitinsutrave.stoptheball.worlds.TutorialWorld;

public class TutorialScreen extends BaseScreen {

    private final StopTheBall stopTheBall;
    private final TutorialWorld world;

    public TutorialScreen(StopTheBall stopTheBall) {
        super();

        this.stopTheBall = stopTheBall;

        world = new TutorialWorld(gameWidth, gameHeight);
    }

    @Override
    public void render(float delta) {
        // Update the state of the objects
        world.update(delta);

        // Set the background color
        Gdx.gl.glClearColor(GameConstants.black.r, GameConstants.black.g, GameConstants.black.b, GameConstants.black.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Start the batcher
        batcher.begin();

        // Draw the ball
        batcher.setColor(GameConstants.yellow);
        batcher.draw(AssetLoader.circle,
                world.getBall().getX()-world.getBall().getRadius(),
                world.getBall().getY()-world.getBall().getRadius(),
                world.getBall().getRadius()*2,
                world.getBall().getRadius()*2);

        // Draw the coin
        batcher.setColor(GameConstants.orange);
        batcher.draw(AssetLoader.circle,
                world.getCoin().getX()-world.getCoin().getRadius(),
                world.getCoin().getY()-world.getCoin().getRadius(),
                world.getCoin().getRadius()*2,
                world.getCoin().getRadius()*2);

        // Draw the user instruction if required
        if (world.isUserInstructionState()) {
            AssetLoader.font.getData().setScale(6.5f, 6.5f);
            AssetLoader.font.setColor(GameConstants.yellow.r, GameConstants.yellow.g, GameConstants.yellow.b, GameConstants.yellow.a);
            AssetLoader.font.draw(batcher, "Tap the big ball where indicated", 350, 300);
        }

        // Unset the color and end batcher
        batcher.setColor(GameConstants.white);
        batcher.end();

        // Draw the game border
        int borderWidth = 100;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        shapeRenderer.setColor(GameConstants.yellow);
        shapeRenderer.rectLine(0, 0, 0, gameHeight, borderWidth);
        shapeRenderer.rectLine(0, 0, gameWidth, 0, borderWidth);
        shapeRenderer.rectLine(0, gameHeight, gameWidth, gameHeight, borderWidth);
        shapeRenderer.rectLine(gameWidth, 0, gameWidth, gameHeight, borderWidth);

        // Draw the user instruction if required
        if (world.isIndicatorNeeded()) {
            shapeRenderer.setColor(GameConstants.red);
            shapeRenderer.circle(world.getIndicator().x, world.getIndicator().y, world.getIndicator().radius);
        }

        shapeRenderer.end();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        float touchPositionX = screenX*scaleFactor;
        float touchPositionY = screenY*scaleFactor;

        world.handleTouch(touchPositionX, touchPositionY);

        return true;
    }
}
