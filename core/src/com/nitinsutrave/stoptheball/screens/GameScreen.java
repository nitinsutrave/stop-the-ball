package com.nitinsutrave.stoptheball.screens;

import com.badlogic.gdx.Gdx;
import com.nitinsutrave.stoptheball.StopTheBall;
import com.nitinsutrave.stoptheball.helpers.InputHandler;
import com.nitinsutrave.stoptheball.renderers.GameRenderer;
import com.nitinsutrave.stoptheball.worlds.GameWorld;

public class GameScreen extends BaseScreen {

    private final GameWorld world;
    private final GameRenderer renderer;

    public GameScreen(StopTheBall stopTheBall) {
        super();

        world = new GameWorld(stopTheBall, gameWidth, gameHeight, scaleFactor);
        renderer = new GameRenderer(world);

//        Gdx.input.setInputProcessor(new InputHandler(world));
//        Gdx.input.setInputProcessor(this);
    }

    public void render(float delta) {
        super.render(delta);
        world.update(delta);
        renderer.render();
    }

    public void pause() {
        super.pause();
        world.pauseGame();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        world.handleTouch(screenX, screenY);
        return true;
    }
}
