package com.nitinsutrave.stoptheball.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.nitinsutrave.stoptheball.StopTheBall;
import com.nitinsutrave.stoptheball.helpers.AssetLoader;
import com.nitinsutrave.stoptheball.helpers.HighScoreHelper;
import com.nitinsutrave.stoptheball.utils.GameConstants;

public class HomeScreen extends BaseScreen {

    private final StopTheBall stopTheBall;
    private final HighScoreHelper highScoreHelper;

    public HomeScreen (StopTheBall stopTheBall) {
        super();
        this.stopTheBall = stopTheBall;

        highScoreHelper = new HighScoreHelper();
    }

    @Override
    public void render(float delta) {
        // Set the background color
        Gdx.gl.glClearColor(GameConstants.yellow.r, GameConstants.yellow.g, GameConstants.yellow.b, GameConstants.yellow.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Start the batcher
        batcher.begin();

        // Draw the name of the app
        AssetLoader.font.getData().setScale(13, 13);
        AssetLoader.font.setColor(GameConstants.black.r, GameConstants.black.g, GameConstants.black.b, GameConstants.black.a);
        AssetLoader.font.draw(batcher, "Stop The Ball", 900, 2200);

        // Draw the game options
        AssetLoader.font.getData().setScale(5, 5);
        AssetLoader.font.draw(batcher, "High Score:      " + highScoreHelper.getHighScore(), 2600, 5200);

        // Draw the start button
        batcher.draw(AssetLoader.startButton, 3100, 9000, 1800, 1800);

        // End the batcher
        batcher.end();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        stopTheBall.setGameScreen();
        return true;
    }
}
