package com.nitinsutrave.stoptheball.gameobjects;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.nitinsutrave.stoptheball.utils.GameConstants;

public class Ball {
    private Vector2 position;
    private Vector2 velocity;
    private Vector2 tempVector;

    private int radius;
    private int velocityMagnitude;

    private Circle boundingCircle;

    public Ball (float x, float y) {
        boundingCircle = new Circle();
        resetToPosition(x, y);
    }

    public void resetToPosition(float x, float y) {
        radius = GameConstants.ballRadiusNormal;
        velocityMagnitude = GameConstants.velocityVerySlow;

        position = new Vector2(x, y);
        velocity = new Vector2(0, velocityMagnitude);

        boundingCircle.set(position.x, position.y, radius);
    }

    public void update(float delta) {
        tempVector = velocity.cpy();
        tempVector.scl(delta);
        position.add(tempVector);

        boundingCircle.setPosition(position.x, position.y);
    }

    public boolean onClick(int screenX, int screenY) {
        // Return if click wasn't on the ball
        if (!boundingCircle.contains(screenX, screenY)) {
            return false;
        }

        // Shift co-ordinates to bring ball centre to origin
        float transformedX = screenX - position.x;
        float transformedY = screenY - position.y;

        // Bring to first quadrant for simplicity of calculations
        if (transformedX < 0) {
            transformedX = -transformedX;
        }
        if (transformedY < 0) {
            transformedY = -transformedY;
        }

        // Get the x and y components of new velocity
        float angle = MathUtils.atan2(transformedY, transformedX);
        float newVelocityX = MathUtils.cos(angle) * velocityMagnitude;
        float newVelocityY = MathUtils.sin(angle) * velocityMagnitude;

        // Set the new velocity in the right direction
        if (screenY > position.y) {
            velocity.y = -newVelocityY;
        } else {
            velocity.y = newVelocityY;
        }

        if (screenX > position.x) {
            velocity.x = -newVelocityX;
        } else {
            velocity.x = newVelocityX;
        }

        return true;
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getRadius() {
        return radius;
    }

    public Circle getBoundingCircle() {
        return boundingCircle;
    }

    public void setPosition(float positionX, float positionY) {
        position.set(positionX, positionY);
        boundingCircle.setPosition(positionX, positionY);
    }

    public void setRadiusBig() {
        radius = GameConstants.ballRadiusBig;
        boundingCircle.setRadius(radius);
    }

    public void setRadiusNormal() {
        radius = GameConstants.ballRadiusNormal;
        boundingCircle.setRadius(radius);
    }

    public void updateVelocity(int newVelocityMagnitude) {
        velocity.scl(newVelocityMagnitude*1.0f/velocityMagnitude);
        velocityMagnitude = newVelocityMagnitude;
    }

    public void setVelocityMagnitude(int newVelocityMagnitude) {
        velocityMagnitude = newVelocityMagnitude;
    }

    public void setVelocityMedium() { updateVelocity(GameConstants.velocityMedium); }

    public void setVelocityVerySlow() {
        updateVelocity(GameConstants.velocityVerySlow);
    }
}
