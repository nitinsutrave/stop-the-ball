package com.nitinsutrave.stoptheball.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.nitinsutrave.stoptheball.utils.GameConstants;
import com.nitinsutrave.stoptheball.worlds.GameWorld;

public class Token {
    private final int radius;
    private final TokenType[] tokens;

    private boolean isActive;

    private Vector2 position;

    private Circle boundingCircle;

    public enum TokenType {
        BALL_SPEED, BALL_SIZE, BONUS_POINTS
    }


    private TokenType tokenType;

    public Token() {
        radius = GameConstants.tokenRadius;
        position = new Vector2();
        boundingCircle = new Circle();
        boundingCircle.setRadius(radius);

        tokens = TokenType.values();

        // Position the token to out of game bounds
        // We will bring it into the game area when required
        removeFromScreen();
    }

    public Circle getBoundingCircle() {
        return boundingCircle;
    }

    public void setPosition(float positionX, float positionY) {
        position.set(positionX, positionY);
        boundingCircle.setPosition(position.x, position.y);
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public int getRadius() {
        return radius;
    }

    public boolean isActive() { return isActive; }

    public void makeActive() {
        isActive = true;
    }

    public void insertRandomToken(float positionX, float positionY) {
        tokenType = tokens[MathUtils.random(2)];
        setPosition(positionX, positionY);
        makeActive();
    }

    public void removeFromScreen() {
        position.x = -6000;
        position.y = -6000;
        boundingCircle.setPosition(-6000, -6000);
        isActive = false;
    }

    public Color getTokenColor() {
        switch (tokenType) {
            case BALL_SIZE: return GameConstants.blue;
            case BALL_SPEED: return GameConstants.red;
            case BONUS_POINTS: return  GameConstants.purple;
            default: return GameConstants.black;
        }
    }

    public void performCollect(GameWorld world) {
        switch (tokenType) {
            case BALL_SIZE: world.getBall().setRadiusBig(); break;
            case BALL_SPEED: world.getBall().setVelocityVerySlow(); break;
            case BONUS_POINTS: world.activateBonusPoints(); break;
        }
        removeFromScreen();
    }
}
