package com.nitinsutrave.stoptheball.gameobjects;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.nitinsutrave.stoptheball.utils.GameConstants;

public class Coin {
    private final int radius;

    private Vector2 position;

    private Circle boundingCircle;

    public Coin() {
        radius = GameConstants.coinRadius;

        position = new Vector2();

        boundingCircle = new Circle();
        boundingCircle.setRadius(radius);
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public int getRadius() {
        return radius;
    }

    public Circle getBoundingCircle() {
        return boundingCircle;
    }

    public void setPosition(float positionX, float positionY) {
        position.set(positionX, positionY);
        boundingCircle.setPosition(position.x, position.y);
    }
}
