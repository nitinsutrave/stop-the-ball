package com.nitinsutrave.stoptheball;

import com.badlogic.gdx.Game;
import com.nitinsutrave.stoptheball.screens.GameScreen;
import com.nitinsutrave.stoptheball.screens.HomeScreen;
import com.nitinsutrave.stoptheball.screens.TutorialScreen;

public class StopTheBall extends Game {

	private HomeScreen homeScreen;
	private GameScreen gameScreen;
	private TutorialScreen tutorialScreen;

	@Override
	public void create() {
		homeScreen = new HomeScreen(this);
		gameScreen = new GameScreen(this);
		tutorialScreen = new TutorialScreen(this);

		homeScreen.load();
		setScreen(homeScreen);
	}

	public void setGameScreen() {
		gameScreen.load();
		setScreen(gameScreen);
	}

	public void setTutorialScreen() {
		tutorialScreen.load();
		setScreen(tutorialScreen);
	}
}
