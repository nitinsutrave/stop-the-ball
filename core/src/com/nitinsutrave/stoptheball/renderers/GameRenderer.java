package com.nitinsutrave.stoptheball.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.nitinsutrave.stoptheball.helpers.AssetLoader;
import com.nitinsutrave.stoptheball.helpers.HighScoreHelper;
import com.nitinsutrave.stoptheball.utils.GameConstants;
import com.nitinsutrave.stoptheball.worlds.GameWorld;

public class GameRenderer {
    private final GameWorld myWorld;
    private final OrthographicCamera cam;
    private final ShapeRenderer shapeRenderer;
    private final SpriteBatch batcher;
    private final BitmapFont bitmapFont;
    private final HighScoreHelper highScoreHelper;

    public GameRenderer(GameWorld world) {
        myWorld = world;

        cam = new OrthographicCamera();
        cam.setToOrtho(true, world.getGameWidth(), world.getGameHeight());

        batcher = new SpriteBatch();
        batcher.setProjectionMatrix(cam.combined);

        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(cam.combined);

        highScoreHelper = new HighScoreHelper();

        bitmapFont = new BitmapFont(true);
    }

    public void render() {
        switch (myWorld.getGameState()) {
            case START: renderGameStart(); return;
            case RUNNING: renderGameRunning(); return;
            case GAME_OVER: renderGameOver(); return;
            case PAUSED: renderGamePaused(); return;
        }
        return;
    }

    private void renderGameStart() {
        // Set the background color
        Gdx.gl.glClearColor(GameConstants.yellow.r, GameConstants.yellow.g, GameConstants.yellow.b, GameConstants.yellow.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Start the batcher
        batcher.begin();

        // Draw the name of the app
        AssetLoader.font.getData().setScale(13, 13);
        AssetLoader.font.setColor(GameConstants.black.r, GameConstants.black.g, GameConstants.black.b, GameConstants.black.a);
        AssetLoader.font.draw(batcher, "Stop The Ball", 900, 2200);

        // Draw the rules for the game
        AssetLoader.font.getData().setScale(5, 5);
        AssetLoader.font.draw(batcher, "Tap on the ball to navigate it", 1550, 5300);
        AssetLoader.font.draw(batcher, "Collect coins to score points", 1600, 5800);
        AssetLoader.font.draw(batcher, "Keep an eye for power ups", 1700, 6300);
//        AssetLoader.font.draw(batcher, "Stop the ball from escaping", 1700, 6800);

        // Draw the start button
        batcher.draw(AssetLoader.startButton, 3100, 9000, 1800, 1800);

        // End the batcher
        batcher.end();
    }

    private void renderGameRunning() {
        // Set the background color
        Gdx.gl.glClearColor(GameConstants.black.r, GameConstants.black.g, GameConstants.black.b, GameConstants.black.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Start the batcher
        batcher.begin();

        // Draw the background
        batcher.draw(AssetLoader.background, 0, 0, myWorld.getGameWidth(), myWorld.getGameHeight());

        // Draw the ball
        batcher.setColor(GameConstants.yellow);
        batcher.draw(AssetLoader.circle,
                myWorld.getBall().getX()-myWorld.getBall().getRadius(),
                myWorld.getBall().getY()-myWorld.getBall().getRadius(),
                myWorld.getBall().getRadius()*2,
                myWorld.getBall().getRadius()*2);

        // Draw the coin
        batcher.setColor(GameConstants.orange);
        batcher.draw(AssetLoader.circle,
                myWorld.getCoin().getX()-myWorld.getCoin().getRadius(),
                myWorld.getCoin().getY()-myWorld.getCoin().getRadius(),
                myWorld.getCoin().getRadius()*2,
                myWorld.getCoin().getRadius()*2);

        // Draw the token if active
        if (myWorld.getToken().isActive()) {
            batcher.setColor(myWorld.getToken().getTokenColor());
            batcher.draw(AssetLoader.circle,
                    myWorld.getToken().getX()-myWorld.getToken().getRadius(),
                    myWorld.getToken().getY()-myWorld.getToken().getRadius(),
                    myWorld.getToken().getRadius()*2,
                    myWorld.getToken().getRadius()*2);
        }

        // Draw the score
        AssetLoader.font.getData().setScale(6.5f, 6.5f);
        AssetLoader.font.setColor(GameConstants.yellow.r, GameConstants.yellow.g, GameConstants.yellow.b, GameConstants.yellow.a);
        AssetLoader.font.draw(batcher, String.format("%03d", myWorld.getScore()), 250, 300);

        if (myWorld.isBonusPointActive()) {
            AssetLoader.font.getData().setScale(6.5f, 6.5f);
            AssetLoader.font.setColor(GameConstants.purple.r, GameConstants.purple.g, GameConstants.purple.b, GameConstants.purple.a);
            AssetLoader.font.draw(batcher, "x2", myWorld.getGameWidth()-900, 300);
        }

        // Unset the color and end batcher
        batcher.setColor(GameConstants.white);
        batcher.end();

        // Draw the game border
        int borderWidth = 100;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(GameConstants.yellow);
        shapeRenderer.rectLine(0, 0, 0, myWorld.getGameHeight(), borderWidth);
        shapeRenderer.rectLine(0, 0, myWorld.getGameWidth(), 0, borderWidth);
        shapeRenderer.rectLine(0, myWorld.getGameHeight(), myWorld.getGameWidth(), myWorld.getGameHeight(), borderWidth);
        shapeRenderer.rectLine(myWorld.getGameWidth(), 0, myWorld.getGameWidth(), myWorld.getGameHeight(), borderWidth);
        shapeRenderer.end();
    }

    private void renderGameOver() {
        // First draw the current game state
        // We will draw the game over message over that
        renderGameRunning();

        // Draw a translucent layer over the game
        Gdx.gl.glEnable(GL20.GL_BLEND);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(GameConstants.yellow.r, GameConstants.yellow.g, GameConstants.yellow.b, 0.09f);
        shapeRenderer.rect(0, 0, myWorld.getGameWidth(), myWorld.getGameHeight());
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);

        // Draw a box to hold the game over message
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(GameConstants.yellow.r, GameConstants.yellow.g, GameConstants.yellow.b, GameConstants.yellow.a);
        shapeRenderer.rect(1200, 2600, 5600, 6000);
        shapeRenderer.end();

        // Start the batcher
        batcher.begin();

        // Draw Game Over
        AssetLoader.font.getData().setScale(8, 8);
        AssetLoader.font.setColor(GameConstants.black.r, GameConstants.black.g, GameConstants.black.b, GameConstants.black.a);
        AssetLoader.font.draw(batcher, "GAME OVER", 2000, 2900);

        // Draw game scores
        AssetLoader.font.getData().setScale(5, 5);
        AssetLoader.font.draw(batcher, "Score:     " + myWorld.getScore(), 2600, 4500);
        AssetLoader.font.draw(batcher, "Best:      " + highScoreHelper.getHighScore(), 2600, 5200);

        // Draw the replay button
        batcher.draw(AssetLoader.replayButton, 3200, 6600, 1600, 1600);

        // End the batcher
        batcher.end();
    }

    private void renderGamePaused() {
        // First draw the current game state
        // We will draw the game paused message over that
        renderGameRunning();

        // Draw a translucent layer over the game
        Gdx.gl.glEnable(GL20.GL_BLEND);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(GameConstants.yellow.r, GameConstants.yellow.g, GameConstants.yellow.b, 0.09f);
        shapeRenderer.rect(0, 0, myWorld.getGameWidth(), myWorld.getGameHeight());
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);

        // Draw a box to hold the game over message
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(GameConstants.yellow.r, GameConstants.yellow.g, GameConstants.yellow.b, GameConstants.yellow.a);
        shapeRenderer.rect(1200, 2600, 5600, 6000);
        shapeRenderer.end();

        // Start the batcher
        batcher.begin();

        // Draw Game Over
        AssetLoader.font.getData().setScale(8, 8);
        AssetLoader.font.setColor(GameConstants.black.r, GameConstants.black.g, GameConstants.black.b, GameConstants.black.a);
        AssetLoader.font.draw(batcher, "GAME PAUSED", 1550, 2900);

        // Draw game scores
        AssetLoader.font.getData().setScale(5, 5);
        AssetLoader.font.draw(batcher, "Tap to continue", 2700, 4500);

        // Draw the replay button
        batcher.draw(AssetLoader.startButton, 3200, 6600, 1600, 1600);

        // End the batcher
        batcher.end();
    }
}
