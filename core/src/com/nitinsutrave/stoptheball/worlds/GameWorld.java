package com.nitinsutrave.stoptheball.worlds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.nitinsutrave.stoptheball.StopTheBall;
import com.nitinsutrave.stoptheball.gameobjects.Ball;
import com.nitinsutrave.stoptheball.gameobjects.Coin;
import com.nitinsutrave.stoptheball.gameobjects.Token;
import com.nitinsutrave.stoptheball.helpers.AssetLoader;
import com.nitinsutrave.stoptheball.helpers.HighScoreHelper;
import com.nitinsutrave.stoptheball.helpers.PositionHelper;
import com.nitinsutrave.stoptheball.utils.GameConstants;

public class GameWorld {
    private final int gameWidth;
    private final int gameHeight;
    private final int midPointX;
    private final int midPointY;
    private final int dummyBigNumber;

    private final float scaleFactor;

    private final Ball ball;
    private final Coin coin;
    private final Token token;

    private final StopTheBall stopTheBallClass;

    private float nextTokenAfter;
    private float removeTokenAfter;
    private float deactivatePowerUpAfter;

    private int score;
    private int scoringFactor;

    private float gameDuration;

    private GameState currentState;
    private HighScoreHelper highScoreHelper;

    public enum GameState {
        START, RUNNING, PAUSED, GAME_OVER
    }

    public GameWorld(StopTheBall stopTheBall, int gameWidth, int gameHeight, float scaleFactor) {
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
        this.scaleFactor = scaleFactor;

        stopTheBallClass = stopTheBall;

        midPointX = gameWidth / 2;
        midPointY = gameHeight / 2;

        dummyBigNumber = 99999;

        currentState = GameState.START;

        ball = new Ball(midPointX, midPointY);
        token = new Token();
        coin = new Coin();
        Vector2 newCoinPosition = PositionHelper.getNewCoinPosition(this, ball, token);
        coin.setPosition(newCoinPosition.x, newCoinPosition.y);
        score = 0;
        scoringFactor = 1;

        nextTokenAfter = GameConstants.firstTokenTime;
        removeTokenAfter = dummyBigNumber;
        deactivatePowerUpAfter = dummyBigNumber;

        gameDuration = 0;

        highScoreHelper = new HighScoreHelper();

        AssetLoader.load();
    }

    public void update(float delta) {
        if (currentState != GameState.RUNNING) {
            return ;
        }

        gameDuration += delta;

        // Update ball speed on game duration
        if (gameDuration > 15 && gameDuration < 16) {
            ball.setVelocityMagnitude(GameConstants.velocityMedium);
        } else if (gameDuration > 6 && gameDuration < 7) {
            ball.setVelocityMagnitude(GameConstants.velocitySlow);
        }

        if (gameDuration > nextTokenAfter) {
            removeTokenAfter = gameDuration + 5;
            nextTokenAfter = gameDuration + 12;
            Vector2 newTokenPosition = PositionHelper.getNewTokenPosition(this, ball, coin);
            token.insertRandomToken(newTokenPosition.x, newTokenPosition.y);
        }

        if (gameDuration > removeTokenAfter) {
            removeTokenAfter = dummyBigNumber;
            token.removeFromScreen();
        }

        if (gameDuration > deactivatePowerUpAfter) {
            deactivatePowerUpAfter = dummyBigNumber;
            scoringFactor = 1;
            ball.setRadiusNormal();
            ball.setVelocityMedium();
        }

        ball.update(delta);

        // Check for game over
        // Game is over if ball goes out of the screen area
        if (!PositionHelper.isBallOnScreen(gameWidth, gameHeight, ball)) {
            currentState = GameState.GAME_OVER;
            highScoreHelper.setHighScore(score);
            return;
        }

        // Check for collection of coins
        if(Intersector.overlaps(ball.getBoundingCircle(), coin.getBoundingCircle())) {
            AssetLoader.coinSound.stop();
            AssetLoader.tokenSound.stop();

            AssetLoader.coinSound.play();
            score += 1*scoringFactor;
            Vector2 newCoinPosition = PositionHelper.getNewCoinPosition(this, ball, token);
            coin.setPosition(newCoinPosition.x, newCoinPosition.y);
        }

        if (Intersector.overlaps(ball.getBoundingCircle(), token.getBoundingCircle())) {
            AssetLoader.coinSound.stop();
            AssetLoader.tokenSound.stop();

            AssetLoader.tokenSound.play();
            token.performCollect(this);
            deactivatePowerUpAfter = gameDuration + 7;
            nextTokenAfter = gameDuration + 10;
        }
    }

    public Ball getBall() {
        return ball;
    }

    public Coin getCoin() {
        return coin;
    }

    public Token getToken() {
        return token;
    }

    public float getGameWidth() {
        return gameWidth;
    }

    public float getGameHeight() {
        return gameHeight;
    }

    public int getScore() {
        return score;
    }

    public GameState getGameState() {
        return currentState;
    }

    public void handleTouch(int screenX, int screenY) {
        float touchPositionX = screenX*scaleFactor;
        float touchPositionY = screenY*scaleFactor;

        switch (currentState) {
            case START: {
                if (touchPositionX>3100 && touchPositionX<4900
                        && touchPositionY>9000 && touchPositionY<10800) {
                    currentState = GameState.RUNNING;
                }
                break;
            }
            case RUNNING: {
                ball.onClick((int) touchPositionX, (int) touchPositionY);
                break;
            }
            case GAME_OVER: {
                if (touchPositionX>3200 && touchPositionX<4800
                        && touchPositionY>6600 && touchPositionY<8200) {
                    resetGame();
                    currentState = GameState.RUNNING;
                }
                break;
            }
            case PAUSED: {
                if (touchPositionX>3200 && touchPositionX<4800
                        && touchPositionY>6600 && touchPositionY<8200) {
                    currentState = GameState.RUNNING;
                }
                break;
            }
        }
    }

    public void handleBackPress() {}

    private void resetGame() {
        ball.resetToPosition(midPointX, midPointY);
        coin.setPosition(midPointX, midPointY);
        token.removeFromScreen();
        score = -1;
        gameDuration = 0;
        scoringFactor = 1;
        nextTokenAfter = 17;
        removeTokenAfter = dummyBigNumber;
        deactivatePowerUpAfter = dummyBigNumber;
        ball.setRadiusNormal();
    }

    public void pauseGame() {
        if (currentState == GameState.RUNNING) {
            currentState = GameState.PAUSED;
        }
    }

    public void activateBonusPoints() {
        scoringFactor = 2;
    }

    public boolean isBonusPointActive() {
        return scoringFactor == 2;
    }
}
