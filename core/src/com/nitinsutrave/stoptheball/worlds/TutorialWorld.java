package com.nitinsutrave.stoptheball.worlds;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.nitinsutrave.stoptheball.gameobjects.Ball;
import com.nitinsutrave.stoptheball.gameobjects.Coin;
import com.nitinsutrave.stoptheball.helpers.AssetLoader;
import com.nitinsutrave.stoptheball.helpers.PositionHelper;

public class TutorialWorld {

    private final int gameWidth;
    private final int gameHeight;

    private final Ball ball;
    private final Coin coin;

    private final Circle indicator;

    private TutorialState currentState;

    private enum TutorialState {
        BALL_MOVING, USER_INSTRUCTION, TRY_AGAIN
    }

    public TutorialWorld (int gameWidth, int gameHeight) {
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;

        ball = new Ball(gameWidth / 2, gameHeight / 2);

        coin = new Coin();
        Vector2 newCoinPosition = PositionHelper.getNewCoinPosition(gameWidth, gameHeight, ball);
        coin.setPosition(newCoinPosition.x, newCoinPosition.y);

        Vector2 indicatorPosition = PositionHelper.getIndicatorPosition(ball, coin);
        indicator = new Circle(indicatorPosition.x, indicatorPosition.y, 200);

        currentState = TutorialState.USER_INSTRUCTION;
    }

    public void update(float delta) {
        if (currentState != TutorialState.BALL_MOVING) {
            return ;
        }

        // Update ball position
        ball.update(delta);

        // Handle ball going out of screen
        if (!PositionHelper.isBallOnScreen(gameWidth, gameHeight, ball)) {
            currentState = TutorialState.TRY_AGAIN;

            ball.setPosition(gameWidth / 2, gameHeight / 2);
            Vector2 newCoinPosition = PositionHelper.getNewCoinPosition(gameWidth, gameHeight, ball);
            coin.setPosition(newCoinPosition.x, newCoinPosition.y);

            Vector2 indicatorPosition = PositionHelper.getIndicatorPosition(ball, coin);
            indicator.setPosition(indicatorPosition.x, indicatorPosition.y);

            return;
        }

        // Check for collection of coins
        if(Intersector.overlaps(ball.getBoundingCircle(), coin.getBoundingCircle())) {
            AssetLoader.coinSound.stop();
            AssetLoader.coinSound.play();

            Vector2 newCoinPosition = PositionHelper.getNewCoinPosition(gameWidth, gameHeight, ball);
            coin.setPosition(newCoinPosition.x, newCoinPosition.y);

            Vector2 indicatorPosition = PositionHelper.getIndicatorPosition(ball, coin);
            indicator.setPosition(indicatorPosition.x, indicatorPosition.y);

            currentState = TutorialState.USER_INSTRUCTION;
        }
    }

    public void handleTouch(float touchPositionX, float touchPositionY) {
        boolean clickOnBall = ball.onClick((int) touchPositionX, (int) touchPositionY);

        if (clickOnBall) {
            currentState = TutorialState.BALL_MOVING;
        }
    }

    public Ball getBall() {
        return ball;
    }

    public Coin getCoin() {
        return coin;
    }

    public Circle getIndicator() {
        return indicator;
    }

    public boolean isUserInstructionState() {
        return currentState == TutorialState.USER_INSTRUCTION;
    }

    public boolean isIndicatorNeeded() {
        return currentState == TutorialState.USER_INSTRUCTION || currentState == TutorialState.TRY_AGAIN;
    }
}
