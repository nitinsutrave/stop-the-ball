package com.nitinsutrave.stoptheball.helpers;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.nitinsutrave.stoptheball.gameobjects.Ball;
import com.nitinsutrave.stoptheball.gameobjects.Coin;
import com.nitinsutrave.stoptheball.gameobjects.Token;
import com.nitinsutrave.stoptheball.worlds.GameWorld;

public class PositionHelper {

    private static int coinTokenBoundary = 500;

    public static boolean isBallOnScreen(int gameWidth, int gameHeight, Ball ball) {
        return ball.getX() > -ball.getRadius()
                && ball.getX() < gameWidth+ball.getRadius()
                && ball.getY() > -ball.getRadius()
                && ball.getY() < gameHeight+ball.getRadius();
    }

    public static Vector2 getNewCoinPosition(GameWorld gameWorld, Ball ball, Token token) {
        float newCoinPositionX = MathUtils.random(coinTokenBoundary, gameWorld.getGameWidth()-coinTokenBoundary);
        float newCoinPositionY = MathUtils.random(coinTokenBoundary, gameWorld.getGameHeight()-coinTokenBoundary);

        while (ball.getBoundingCircle().contains(newCoinPositionX, newCoinPositionY)
                || (token.isActive() && token.getBoundingCircle().contains(newCoinPositionX, newCoinPositionY))) {
            newCoinPositionX = MathUtils.random(coinTokenBoundary, gameWorld.getGameWidth()-coinTokenBoundary);
            newCoinPositionY = MathUtils.random(coinTokenBoundary, gameWorld.getGameHeight()-coinTokenBoundary);
        }

        return new Vector2(newCoinPositionX, newCoinPositionY);
    }

    public static Vector2 getNewCoinPosition(int gameWidth, int gameHeight, Ball ball) {
        float newCoinPositionX = MathUtils.random(coinTokenBoundary, gameWidth-coinTokenBoundary);
        float newCoinPositionY = MathUtils.random(coinTokenBoundary, gameHeight-coinTokenBoundary);

        while (ball.getBoundingCircle().contains(newCoinPositionX, newCoinPositionY)) {
            newCoinPositionX = MathUtils.random(coinTokenBoundary, gameWidth-coinTokenBoundary);
            newCoinPositionY = MathUtils.random(coinTokenBoundary, gameHeight-coinTokenBoundary);
        }

        return new Vector2(newCoinPositionX, newCoinPositionY);
    }

    public static Vector2 getNewTokenPosition(GameWorld gameWorld, Ball ball, Coin coin) {
        float newCoinPositionX = MathUtils.random(coinTokenBoundary, gameWorld.getGameWidth()-coinTokenBoundary);
        float newCoinPositionY = MathUtils.random(coinTokenBoundary, gameWorld.getGameHeight()-coinTokenBoundary);

        while (ball.getBoundingCircle().contains(newCoinPositionX, newCoinPositionY)
                || coin.getBoundingCircle().contains(newCoinPositionX, newCoinPositionY)) {
            newCoinPositionX = MathUtils.random(coinTokenBoundary, gameWorld.getGameWidth()-coinTokenBoundary);
            newCoinPositionY = MathUtils.random(coinTokenBoundary, gameWorld.getGameHeight()-coinTokenBoundary);
        }

        return new Vector2(newCoinPositionX, newCoinPositionY);
    }

    public static Vector2 getIndicatorPosition(Ball ball, Coin coin) {
        float slope = (ball.getY() - coin.getY())/(ball.getX() - coin.getX());
        float slopeAngle = MathUtils.atan2(slope, 1);

//        Gdx.app.log("anglecalc", ball.getY() +" "+ coin.getY()+" "+ball.getX()+" "+coin.getX());
//        Gdx.app.log("anglecalc", "angle: " + slopeAngle);

        float xOffset = 700*MathUtils.cos(slopeAngle);
        float yOffset = 700*MathUtils.sin(slopeAngle);

        if (xOffset < 0) xOffset = -xOffset;
        if (yOffset < 0) yOffset = -yOffset;

        float indicatorPositionX = 0;
        float indicatorPositionY = 0;

        if (coin.getX() > ball.getX()) {
            indicatorPositionX = ball.getX() - xOffset;
        } else {
            indicatorPositionX = ball.getX() + xOffset;
        }

        if (coin.getY() > ball.getY()) {
            indicatorPositionY = ball.getY() - yOffset;
        } else {
            indicatorPositionY = ball.getY() + yOffset;
        }

        return new Vector2(indicatorPositionX, indicatorPositionY);
    }
}
