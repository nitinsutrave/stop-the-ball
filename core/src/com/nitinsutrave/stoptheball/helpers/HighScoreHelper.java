package com.nitinsutrave.stoptheball.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.nitinsutrave.stoptheball.utils.GameConstants;

public class HighScoreHelper {

    private Preferences prefs;
    private int highScore;

    public HighScoreHelper () {
        prefs = Gdx.app.getPreferences(GameConstants.preferencesKey);

        if (!prefs.contains(GameConstants.prefHighScore)) {
            prefs.putInteger(GameConstants.prefHighScore, 0);
            prefs.flush();
            highScore = 0;
        } else {
            highScore = prefs.getInteger(GameConstants.prefHighScore);
        }
    }

    public int getHighScore() {
        return highScore;
    }

    public boolean setHighScore(int newHighScore) {
        if (newHighScore > highScore) {
            prefs.putInteger(GameConstants.prefHighScore, newHighScore);
            prefs.flush();
            highScore = newHighScore;
            return true;
        }
        return false;
    }
}
