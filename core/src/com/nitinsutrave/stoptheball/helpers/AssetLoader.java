package com.nitinsutrave.stoptheball.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetLoader {
    private static Texture circleTexture;
    private static Texture startButtonTexture;
    private static Texture replayButtonTexture;
    private static Texture backgroundTexture;

    public static TextureRegion circle;
    public static TextureRegion startButton;
    public static TextureRegion replayButton;
    public static TextureRegion background;

    public static BitmapFont font;

    public static Music mainMusic;
    public static Music coinSound;
    public static Music tokenSound;

    public static void load() {
        circleTexture = new Texture(Gdx.files.internal("data/images/circle.png"));
        circle = new TextureRegion(circleTexture, 0, 0, 820, 820);

        startButtonTexture = new Texture(Gdx.files.internal("data/images/start.png"));
        startButton = new TextureRegion(startButtonTexture, 0, 0, 480, 480);

        replayButtonTexture = new Texture(Gdx.files.internal("data/images/replay.png"));
        replayButton = new TextureRegion(replayButtonTexture, 0, 0, 420, 420);
        replayButton.flip(false, true);

        backgroundTexture = new Texture(Gdx.files.internal("data/images/background.png"));
        background = new TextureRegion(backgroundTexture, 0, 0, 1080, 1920);

//        mainMusic = Gdx.audio.newMusic(Gdx.files.internal("data/sounds/loop.wav"));
//        mainMusic.setLooping(true);
//        mainMusic.play();
//
        coinSound = Gdx.audio.newMusic(Gdx.files.internal("data/sounds/coincollect.wav"));

        tokenSound = Gdx.audio.newMusic(Gdx.files.internal("data/sounds/tokencollect.wav"));

        font = new BitmapFont(Gdx.files.internal("data/fonts/font.fnt"), true);
    }
}
