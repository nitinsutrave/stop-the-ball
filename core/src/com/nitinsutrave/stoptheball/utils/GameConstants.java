package com.nitinsutrave.stoptheball.utils;

import com.badlogic.gdx.graphics.Color;

public class GameConstants {
    public static int gameWidth = 8000;

//    public static int velocityVerySlow = 2500;
//    public static int velocitySlow = 3500;
//    public static int velocityMedium = 4500;
//    public static int velocityFast = 5500;

    public static int velocityVerySlow = 250;
    public static int velocitySlow = 350;
    public static int velocityMedium = 450;
    public static int velocityFast = 550;

    public static int firstTokenTime = 17;

    public static int ballRadiusNormal = 1000;
    public static int ballRadiusBig = 1300;
    public static int coinRadius = 200;
    public static int tokenRadius = 200;

    public static Color yellow = new Color(247 / 255.0f, 249 / 255.0f, 16 / 255.0f, 1);
    public static Color black = new Color(0 / 255.0f, 0 / 255.0f, 0 / 255.0f, 1);
    public static Color orange = new Color(236 / 255.0f, 169 / 255.0f, 8 / 255.0f, 1);
    public static Color white = new Color(255 / 255.0f, 255 / 255.0f, 255 / 255.0f, 1);
    public static Color blue = new Color(9 / 255.0f, 120 / 255.0f, 186 / 255.0f, 1);
    public static Color red = new Color(204 / 255.0f, 58 / 255.0f, 8 / 255.0f, 1);
    public static Color purple = new Color(117 / 255.0f, 13 / 255.0f, 197 / 255.0f, 1);

    public static String preferencesKey = "GameData";
    public static String prefHighScore = "highScore";

}
